import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { from } from 'rxjs';
import { PagenotFoundComponent } from './pagenot-found/pagenot-found.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomePageComponent } from './home-page/home-page.component'

const routes: Routes = [
  { path: 'dashboard-component', component: DashboardComponent },
  { path: '', component: HomePageComponent },  // Wildcard route for a 404 page
  { path: '**', component: PagenotFoundComponent },  // Wildcard route for a 404 page
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {


}
