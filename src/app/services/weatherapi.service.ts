import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class WeatherapiService {

  constructor(private http: HttpClient) { }

  apiList = {
    cities : "./assets/json/cities.json",
    weatherApi : "http://api.openweathermap.org/data/2.5/weather?q="
  }


  // get service call 
  getConfig(api: string) {
    return this.http.get(api);
  }
}
