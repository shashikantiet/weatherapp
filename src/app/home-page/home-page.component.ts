import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { from } from 'rxjs';
import { WeatherapiService } from '../services/weatherapi.service'


@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})



export class HomePageComponent implements OnInit {
  cityList: any;


  constructor(private router: Router, private weatherapiService: WeatherapiService) { }

  ngOnInit(): void {
    // get cities list 
    this.getCityList();
  }

  getCityList() {
    let citiesListApi = this.weatherapiService.apiList.cities;
    this.weatherapiService.getConfig(citiesListApi).subscribe((res: any) => {
      this.cityList = res.cities;
    })
  }

  gotoDashboard(cityDetails) {
    this.router.navigate(['/dashboard-component',
      {
        cityName: cityDetails.city,
        countryName: cityDetails.country
      }
    ]);
  }
}
