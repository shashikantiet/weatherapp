import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { from } from 'rxjs';
import { WeatherapiService } from '../services/weatherapi.service'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  cityName: String;
  countryName: String;
  weatherDetail:any;

  constructor(private activatedRoute: ActivatedRoute, private weatherapiService:WeatherapiService) {
  }

  ngOnInit(): void {
    //Get params value from Home page 
    this.cityName = this.activatedRoute.snapshot.paramMap.get('cityName');
    this.countryName = this.activatedRoute.snapshot.paramMap.get('countryName');

    //Get Weather Detail
    this.getWeatherDetailViaCity();
  }

  getWeatherDetailViaCity() {
    let weatherApi = this.weatherapiService.apiList.weatherApi+ this.cityName +',' + this.countryName +'&&appid=3d8b309701a13f65b660fa2c64cdc517';
    this.weatherapiService.getConfig(weatherApi).subscribe((res:any) =>{
      this.weatherDetail = res;
    })
  }

  getDateTime(getDateTime){
      return new Date(getDateTime);
  }
}
